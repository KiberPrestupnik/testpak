define("DlConcertProgramPage", ["DlFrequencyPerformancesConstantsJs"], function(DlFrequencyPerformancesConstants) {
	return {
		entitySchemaName: "DlConcertProgram",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "DlConcertProgramFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "DlConcertProgram"
				}
			},
			"DlPerformanceDetail": {
				"schemaName": "DlPerformanceDetail",
				"entitySchemaName": "DlPerformance",
				"filter": {
					"detailColumn": "DlConcertProgram",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			/**
			 * @override
			 * @inheritdoc BasePageV2#onEntityInitialized
			 */
			onEntityInitialized: function(){
				this.callParent(arguments);
				Terrasoft.ServerChannel.on(Terrasoft.EventName.ON_MESSAGE, this.onServerMessageReceived, this);
			},

			/**
			 * @override
			 * @inheritdoc Terrasoft.BaseSchemaViewModel#destroy
			 */
			destroy: function() {
				Terrasoft.ServerChannel.un(Terrasoft.EventName.ON_MESSAGE, this.onServerMessageReceived, this);
				this.callParent(arguments);
			},

			/**
			 * Обработка получания сообщения
			 * @param {Object} scope Контекст
			 * @param {Object} message Сообщение
			 */
			onServerMessageReceived: function(scope, message) {
				const sender = message?.Header?.Sender;
				if (sender === "ReloadDetail" && message?.Body === "true") {
					this.updateDetail({ detail: "DlPerformanceDetail" });
				}
			},

			/**
			 * @override
			 * @inheritdoc BasePageV2#asyncValidate
			 */
			asyncValidate: function(callback, scope) {
				this.callParent([function() {	
					const validationResult = {
						success: true
					};

					if (this.getLookupValue("DlFrequency") !== DlFrequencyPerformancesConstants.Daily) {
						callback.call(scope || this, validationResult);
						return;
					}
					
					const esq = Ext.create("Terrasoft.EntitySchemaQuery", { 
						rootSchemaName: "DlConcertProgram"
					});
					esq.addAggregationSchemaColumn("Id", Terrasoft.AggregationType.COUNT, "Count");
					esq.filters.addItem(
						esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "DlFrequency", DlFrequencyPerformancesConstants.Daily)
					);
					esq.filters.addItem(
						esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "DlIsActive", true)
					);
					esq.getEntityCollection(function(result) {
						const resultItems = result.collection.getByIndex(0);
						Terrasoft.SysSettings.querySysSettingsItem("DlMaxNumberActiveEveryDayProgram", function(value) {
							if (resultItems.get("Count") === value && this.get("DlIsActive")) {
								if (this.isAddMode() || this.isAttributeChanged("DlIsActive")) {
									validationResult.message = `Ежедневных концертных программ допускается не более ${value}.`;
									validationResult.success = false;
								}
							}
							callback.call(scope || this, validationResult);							
						}, this);
					}, this);
				}, this]);  
			},
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "DlCode",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "DlCode"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DlIsActive",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "DlIsActive"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "DlName",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "DlName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DlGroup",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "DlGroup"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "DlResponsible",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "DlResponsible"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DlFrequency",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "DlFrequency"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DlComment",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "DlComment",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DlPerformancesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.DlPerformancesTabTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DlPerformanceDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "DlPerformancesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "DlNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			}
		]/**SCHEMA_DIFF*/
	};
});
