namespace DL_TestTask
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using Terrasoft.Common;
    using Terrasoft.Configuration;
    using Terrasoft.Core;
    using Terrasoft.Core.DB;
    using Terrasoft.Web.Common;

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class DlConcertProgramService : BaseService
    {
        private readonly Guid planingStatusPerfomanceId = Guid.Parse("79863a30-2f8e-4366-8794-3c125dcfcd56"); // Статус выступления "Запланированный"

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public int GetSumTickets(string input)
        {
            if (!DateTime.TryParse(input, out DateTime result))
                return 0;

            var select = (Select)new Select(UserConnection)
                .Column(Func.Sum(nameof(DlPerformance), nameof(DlPerformance.DlNumberTickets)))
                .From(nameof(DlPerformance))
                .InnerJoin(nameof(DlConcertProgram)).On(nameof(DlConcertProgram), nameof(DlConcertProgram.Id)).IsEqual(nameof(DlPerformance), nameof(DlPerformance.DlConcertProgramId))
                .Where(nameof(DlConcertProgram), nameof(DlConcertProgram.DlIsActive)).IsEqual(Column.Const(true))
                .And(nameof(DlPerformance), nameof(DlPerformance.DlPerformanceStatusId)).IsEqual(Column.Const(planingStatusPerfomanceId))
                .And(nameof(DlPerformance), nameof(DlPerformance.DlPerformanceDate)).IsEqual(Column.Const(result));

            return select.ExecuteScalar<int>();
        }
    }
} 